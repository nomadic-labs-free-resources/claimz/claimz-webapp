import type { NextPage } from 'next';
import { useState } from 'react';
import {
    Box,
    Container,
    Heading,
    Modal,
    ModalContent,
    ModalFooter,
    ModalOverlay,
    Stack,
    Text,
    useDisclosure,
} from '@chakra-ui/react';
import { NetworkType } from '@airgap/beacon-sdk';
import axios from 'axios';
import Confetti from 'react-confetti';

import { useWindowSize, useWindowSizeProps } from '@hooks/useWindowSize';
import { useStore } from '@store/store';
import Button from '@components/Button';
import Success from '@components/Success';
import Error from '@components/Error';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:5000';
const RPC_URL = process.env.NEXT_PUBLIC_RPC_URL || 'http://localhost:8732';
const NETWORK = process.env.NEXT_PUBLIC_NETWORK || 'custom';
const TZKT_API = process.env.NEXT_PUBLIC_TZKT_URL || 'https://api.tzkt.io/v1';

const CONTRACT =
    process.env.NEXT_PUBLIC_CONTRACT || 'KT1D2k8Xdkh322ohfHNLKvMEwfKwFyroPbQf';
const TOKENID = process.env.NEXT_PUBLIC_TOKENID || 0;

const Home: NextPage = () => {
    const wallet = useStore((state) => state.wallet);
    const [success, setSuccess] = useState(false);
    const [rainConffetis, setRainConfettis] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const { width, height }: useWindowSizeProps = useWindowSize();
    const { isOpen, onOpen, onClose } = useDisclosure();

    const displayConfettis = () => {
        setRainConfettis(true);
        setTimeout(() => {
            setRainConfettis(false);
        }, 10000);
    };

    const claimNFT = async (): Promise<void> => {
        try {
            setLoading(true);
            await wallet?.requestPermissions({
                network: {
                    type: NETWORK as NetworkType,
                    rpcUrl: RPC_URL,
                },
            });
            const address = await wallet?.getPKH();
            console.log(address);

            const balance = await axios.get(
                `${TZKT_API}/tokens/balances/count?account=${address}&token.contract=${CONTRACT}&tokenId=${TOKENID}`,
                { headers: { 'Access-Control-Allow-Origin': '*' } }
            );

            if (balance.data === 0) {
                const resp = await axios.post(`/api/operations`, {
                    to_address: address,
                });
                console.log(resp);
                setSuccess(true);
                displayConfettis();
            }
        } catch (error) {
            console.log(error);
            setSuccess(false);
        } finally {
            setLoading(false);
            onOpen();
        }
    };

    return (
        <Box
            h="100vh"
            w="100vw"
            bgColor="#3c73f1"
            bgImage="/HoldingCoinTezos.jpeg"
            bgPosition={{
                base: 'bottom',
                lg: 'bottom left 1px',
            }}
            bgSize={{ base: '225%', md: '150%', xl: '135%' }}
            bgRepeat="no-repeat"
        >
            <Container maxW={'7xl'} h="100%">
                <Stack
                    // align={'center'}
                    justify="center"
                    maxW={{ base: '7xl', md: 'lg' }}
                    spacing={{ base: 8, md: 10 }}
                    py={{ base: 20, md: 28 }}
                    direction={{ base: 'column', md: 'row' }}
                >
                    <Stack flex={1} spacing={{ base: 5, md: 10 }}>
                        <Heading
                            lineHeight={1.1}
                            fontWeight={600}
                            fontSize={{ base: '5xl', sm: '6xl', lg: '6xl' }}
                        >
                            <Text
                                as={'span'}
                                position={'relative'}
                                fontWeight="bold"
                                color={'white'}
                            >
                                Claim your
                            </Text>
                            <br />
                            <Text
                                as={'span'}
                                color={'#1D2227'}
                                fontWeight="bold"
                            >
                                NFT!
                            </Text>
                        </Heading>
                        <Text color={'gray.300'}>
                            Click on the button, connect to a wallet and get an
                            NFT!
                        </Text>
                        <Stack
                            spacing={{ base: 4, sm: 6 }}
                            // direction={{ base: 'column', sm: 'row' }}
                        >
                            <Button onClick={claimNFT} isLoading={isLoading}>
                                Claim
                            </Button>
                            {rainConffetis && (
                                <Confetti width={width} height={height} />
                            )}
                            <Modal
                                closeOnOverlayClick={true}
                                isOpen={isOpen}
                                onClose={onClose}
                                size="xs"
                            >
                                <ModalOverlay />
                                <ModalContent>
                                    <Container>
                                        {success ? <Success /> : <Error />}
                                    </Container>
                                    <ModalFooter>
                                        <Button onClick={onClose}>
                                            Got it!
                                        </Button>
                                    </ModalFooter>
                                </ModalContent>
                            </Modal>
                        </Stack>
                    </Stack>
                </Stack>
            </Container>
        </Box>
    );
};

export default Home;
