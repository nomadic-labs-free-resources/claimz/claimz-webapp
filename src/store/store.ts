import create from 'zustand';
import { BeaconWallet } from '@taquito/beacon-wallet';
import { NetworkType } from '@airgap/beacon-sdk';

interface Store {
    wallet: BeaconWallet | null;
    address: string | null;
    setAddress: (address: string) => void;
}

const client =
    typeof window !== 'undefined'
        ? new BeaconWallet({
              name: 'Claimz',
              preferredNetwork: NetworkType.MAINNET,
          })
        : null;

export const useStore = create<Store>((set) => ({
    wallet: client,
    address: null,
    setAddress: (address: string) => set({ address }),
}));
