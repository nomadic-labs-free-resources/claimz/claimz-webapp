import {
    Button as ChakraButton,
    ButtonProps as ChakraButtonProps,
} from '@chakra-ui/react';
import { ReactNode } from 'react';

interface ButtonProps extends ChakraButtonProps {
    children: ReactNode;
}

export const Button = (props: ButtonProps) => {
    return (
        <ChakraButton
            {...props}
            px={4}
            fontSize={'xl'}
            rounded={'full'}
            bg={'#1D2227'}
            color={'white'}
            boxShadow={
                '0px 1px 25px -5px rgb(29 34 39 / 48%), 0 10px 10px -5px rgb(29 34 39 / 43%)'
            }
            _hover={{
                bg: '#030405',
            }}
            _focus={{
                bg: '#030405',
            }}
        >
            {props.children}
        </ChakraButton>
    );
};

export default Button;
