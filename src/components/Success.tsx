import { FC } from 'react';
import { Box, Heading, Text } from '@chakra-ui/react';
import { CheckCircleIcon } from '@chakra-ui/icons';

export const Success: FC = () => {
    return (
        <Box textAlign="center" py={10} px={6}>
            <CheckCircleIcon boxSize={'50px'} color={'green.500'} />
            <Heading as="h2" size="xl" mt={6} mb={2}>
                Your NFT is on its way
            </Heading>
            <Text color={'gray.500'}>
                Wait a few minutes and check your wallet!
            </Text>
        </Box>
    );
};

export default Success;
